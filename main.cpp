#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QHotkey *hotkey = new QHotkey(QKeySequence("ctrl+alt+S"), true, &a);//The hotkey will be automatically registered
    //qDebug() << "Is Registered: " << hotkey->isRegistered();

    MainWindow w;
    QObject::connect(hotkey, &QHotkey::activated, qApp, [&](){
        //qDebug() << "Hotkey pressed, screenshootin'";
        w.screenShot();
    });
    //w.show();

    return a.exec();
}
