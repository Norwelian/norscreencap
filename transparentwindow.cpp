#include "transparentwindow.h"

TransparentWindow::TransparentWindow(QWidget *parent, QPixmap *originalPixmap) : QDialog(parent)
{
    if(originalPixmap){
        rubbah = nullptr;
        orig_bmp = originalPixmap;
        setAttribute(Qt::WA_NoSystemBackground, true);
        setAttribute(Qt::WA_TranslucentBackground, true);
        setWindowOpacity(0.75);

        QLabel *full_im = new QLabel(this);
        full_im->setPixmap(*originalPixmap);
        full_im->setAttribute(Qt::WA_NoSystemBackground, true);
        full_im->setAttribute(Qt::WA_TranslucentBackground, true);
        full_im->setWindowOpacity(0.75);

        QShortcut *close_window = new QShortcut(QKeySequence(tr("ESC")), this);
        connect(close_window, SIGNAL(activated()), this, SLOT(close()));

        QShortcut *fullscreen = new QShortcut(QKeySequence(Qt::Key_Return), this);
        connect(fullscreen, SIGNAL(activated()), this, SLOT(full_capture()));

        showFullScreen();
        raise();
        setFocus();
    }
}

void TransparentWindow::mousePressEvent(QMouseEvent *event){
     t = event->pos();
     b = event->pos();
     if (!rubbah){
        rubbah = new QRubberBand(QRubberBand::Rectangle, this);
     }
     rubbah->setGeometry(QRect(t, b));
     rubbah->show();
}

 void TransparentWindow::mouseMoveEvent(QMouseEvent *event) {
    b = event->pos();
    rubbah->setGeometry(QRect(t, b));
 }

 void TransparentWindow::mouseReleaseEvent(QMouseEvent *event) {
    b = event->pos();
    rubbah->hide();
    const QRect & selectRect = rubbah->geometry();
    int x, y, width, height;
    selectRect.getRect(&x, &y, &width, &height);
    QPixmap tmp = orig_bmp->copy(x, y, width, height);
    qobject_cast<MainWindow*>(parent())->set_img(tmp);
    close();
    this->deleteLater();
 }

 void TransparentWindow::full_capture(){
     qobject_cast<MainWindow*>(parent())->set_img(*orig_bmp);
     close();
     this->deleteLater();
 }
