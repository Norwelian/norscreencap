#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    show();
    width = ui->label->width();
    height = ui->label->height();

    screen = QGuiApplication::primaryScreen();
    screenz = QGuiApplication::screens();
    for(int i = 0; i < screenz.count(); i++){
        QAction *changeDisplay = new QAction(this);
        changeDisplay->setText(screenz.at(i)->name());
        changeDisplay->setCheckable(true);
        connect(changeDisplay, SIGNAL(triggered(bool)), this, SLOT(screen_change_callback()));
        ui->menuScreens->addAction(changeDisplay);
        if(i == 0){
            changeDisplay->setChecked(true);
        }
    }

    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->save, SIGNAL(clicked(bool)), this, SLOT(save_slot()));
    connect(ui->actionUsage, SIGNAL(triggered()), this, SLOT(help_slot()));
    connect(ui->actionReset_screenshot, SIGNAL(triggered()), this, SLOT(reset_slot()));
    connect(ui->clipboard, SIGNAL(clicked(bool)), this, SLOT(copy_clipboard_slot()));
    qApp->setQuitOnLastWindowClosed(false);
    create_tray_icon();
    hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent* e)
{
    switch (e->type())
    {
        case QEvent::WindowStateChange:
            {
                if (this->windowState() & Qt::WindowMinimized)
                    if (ui->actionAuto_hide_when_minimized->isChecked())
                        QTimer::singleShot(250, this, SLOT(hide()));
                break;
            }
    }
    QMainWindow::changeEvent(e);
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    if(!img.isNull()){
        QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Exit without saving", tr("Are you sure?\n"), QMessageBox::Yes | QMessageBox::No);
        if (resBtn != QMessageBox::Yes)
            event->ignore();
        else {
            event->accept();
            qApp->exit(0);
        }
    } else {
        event->accept();
        qApp->exit(0);
    }
}


void MainWindow::resizeEvent(QResizeEvent *e){
    QMainWindow::resizeEvent(e);
    width = ui->label->width();
    height = ui->label->height();
    if(!img.isNull()){
        img_scaled = img.scaled(width, height, Qt::KeepAspectRatio, Qt::FastTransformation);
        ui->label->setPixmap(img_scaled);
    }
    //qDebug() << "label w: " << width << "label h: " << height << "\noldsize: " << e->oldSize() << "newsize: " << e->size();
    e->accept();
}

void MainWindow::screen_change_callback(){
    QAction* sender_action = qobject_cast<QAction*>(sender());
    QList<QAction*> a = ui->menuScreens->actions();
    QList<QAction*> b = m_tray_icon->contextMenu()->actions();
    for(int i = 0; i < a.count(); i++){
        QAction *tmp = qobject_cast<QAction*>(a.at(i));
        QAction *tmp2 = qobject_cast<QAction*>(b.at(i));
        if(tmp->text() == sender_action->text()){
            tmp->setChecked(true);
            tmp2->setChecked(true);
            screen = screenz.at(i);
        } else {
            tmp2->setChecked(false);
            tmp->setChecked(false);
        }
    }
}

void MainWindow::create_tray_icon()
{
   m_tray_icon = new QSystemTrayIcon(QIcon(":/style/icon.ico"), this);
   connect( m_tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(click_tray_slot(QSystemTrayIcon::ActivationReason)) );

   QAction *quit_action = new QAction( "Exit", m_tray_icon );
   connect( quit_action, SIGNAL(triggered()), this, SLOT(close()) );
   QAction *hide_action = new QAction( "Show/Hide", m_tray_icon );
   connect( hide_action, SIGNAL(triggered()), this, SLOT(hide_btn_slot()));
   QAction *help_action = new QAction( "Usage", m_tray_icon );
   connect( help_action, SIGNAL(triggered()), this, SLOT(help_slot()));

   QMenu *tray_icon_menu = new QMenu;
   for(int i = 0; i < screenz.count(); i++){
       QAction *changeDisplay = new QAction(this);
       changeDisplay->setText(screenz.at(i)->name());
       changeDisplay->setCheckable(true);
       connect(changeDisplay, SIGNAL(triggered(bool)), this, SLOT(screen_change_callback()));
       tray_icon_menu->addAction(changeDisplay);
       if(i == 0){
           changeDisplay->setChecked(true);
       }
   }
   tray_icon_menu->addSeparator();
   tray_icon_menu->addAction( help_action );
   tray_icon_menu->addAction( hide_action );
   tray_icon_menu->addAction( quit_action );

   m_tray_icon->setContextMenu( tray_icon_menu );
   m_tray_icon->setVisible(true);
   m_tray_icon->show();
 }

void MainWindow::click_tray_slot( QSystemTrayIcon::ActivationReason reason )
{
    if( reason != QSystemTrayIcon::DoubleClick )
        return;
    hide_btn_slot();
}

void MainWindow::hide_btn_slot(){
    if(isVisible()){
        QTimer::singleShot(250, this, SLOT(hide()));
    } else if(!img.isNull()){
        show();
        raise();
        setFocus();
        setWindowState(windowState() & ~Qt::WindowMinimized | Qt::WindowActive);
    } else
        QMessageBox::about(this, tr("Error: No screenshot"), tr("You need to make an screenshot first, hotkey:\n\n{ Ctrl+Alt+S }\n\n* For more information, check Usage."));
}

void MainWindow::screenShot(){
    if (!screen)
        return;
    ui->label->clear();
    img = QPixmap(screen->grabWindow(0,screen->geometry().x(), screen->geometry().y(), screen->size().width(), screen->size().height()));
    tww = new TransparentWindow(this, &img);
}

void MainWindow::save_slot(){
    srand((unsigned)time(NULL));
    QString outname = QFileDialog::getSaveFileName(this, tr("Save File"), "screenshot" + QString::number(((rand()+rand()) % 100000) + 10000) + ".png", tr("PNG (*.png)"));
    if(!outname.isEmpty()){
        QFile file(outname);
        file.open(QIODevice::WriteOnly);
        if(!img.isNull())
            img.save(&file, "PNG");
        else
            qDebug() << "Error saving, img is null";
        file.close();
        if(ui->actionCopy_image_to_clipboard->isChecked())
            copy_clipboard_slot();
        ui->label->clear();
        img = QPixmap();
        hide_btn_slot();
        QMessageBox::about(this, tr("Saved successfully"), tr(qPrintable("Screenshot saved as " + outname)));
    }
}

void MainWindow::set_img(QPixmap p){
    if(!p.isNull()){
        img = p;
        img_scaled = img.scaled(width, height, Qt::KeepAspectRatio);
        ui->label->setPixmap(img_scaled);
    }
    show();
    raise();
    setFocus();
    setWindowState(windowState() & ~Qt::WindowMinimized | Qt::WindowActive);
}

void MainWindow::help_slot(){
    QMessageBox::about(this, tr("Help / USAGE"), tr(qPrintable("To make a screenshot, press { Ctrl+Alt+S }.\n\nWhen in screenshot mode:\n  - Select the part of the screen you want\n  - Press { RETURN } to capture the entire screen\n  - Press { ESC } to cancel the screenshot.\n\nTo change the screen, use the `Screens` menu on the top, or in the tray icon.")));
}

void MainWindow::reset_slot(){
    ui->label->clear();
    img = QPixmap();
    hide_btn_slot();
    QMessageBox::about(this, tr("Reset"), tr(qPrintable("Screenshot resetted successfully")));
}

void MainWindow::copy_clipboard_slot(){
    if(!img.isNull())
        qApp->clipboard()->setPixmap(img, QClipboard::Clipboard);
    else
        qDebug() << "Error copying to clipboard, img is null";
}
