#ifndef TRANSPARENTWINDOW_H
#define TRANSPARENTWINDOW_H

#include <QWidget>
#include <QRubberBand>
#include <QShortcut>
#include <QLabel>
#include <QDialog>
#include <QMouseEvent>
#include <QDebug>
#include <QPalette>
#include <mainwindow.h>

class TransparentWindow : public QDialog
{
    Q_OBJECT
public:
    explicit TransparentWindow(QWidget *parent = nullptr, QPixmap *originalPixmap = nullptr);

private:
    QDialog *capture_window;
    QRubberBand *rubbah;
    QPixmap *orig_bmp;
    QPoint t;
    QPoint b;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

signals:

private slots:
    void full_capture();
};

#endif // TRANSPARENTWINDOW_H
