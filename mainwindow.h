#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QHotkey/qhotkey.h>
#include <QScreen>
#include <QPixmap>
#include <QWindow>
#include <QMessageBox>
#include <QCloseEvent>
#include <QFileDialog>
#include <QClipboard>
#include <time.h>
#include <transparentwindow.h>


namespace Ui {
class MainWindow;
}

class TransparentWindow;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void screenShot();
    void set_img(QPixmap i);

private:
    Ui::MainWindow *ui;
    void create_tray_icon();
    void changeEvent(QEvent *);
    void closeEvent(QCloseEvent *);
    void resizeEvent(QResizeEvent *);
    int width, height;
    QScreen *screen;
    QList <QScreen*> screenz;
    TransparentWindow *tww;
    QSystemTrayIcon *m_tray_icon;
    QPixmap img;
    QPixmap img_scaled;

private slots:
    void click_tray_slot( QSystemTrayIcon::ActivationReason reason );
    void save_slot();
    void help_slot();
    void reset_slot();
    void copy_clipboard_slot();
    void screen_change_callback();

public slots:
    void hide_btn_slot();

};

#endif // MAINWINDOW_H
